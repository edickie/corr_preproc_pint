# running PINT permutations

template files for these scripts were created in R and are in perm_templates folder.

## 2017-10-15 The smoothing step 

To be careful about env variable for the smoothing kernal, each one was launched from a different gpc node

This step is repeated with smoothing kernels 4 6 8 10 12

**NOTE: today we learned that wb_command -cifti-smoothing need the whole GPC node to run effectively... (i.e. use -j 1)**

```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422
export SmoothFWHM=12
export SmoothSigma=`echo "${SmoothFWHM} / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`

## create the directory structure
SmoothDIR=${SCRATCH}/CORR/temp_smooth
cat ~/myscripts/corr/pint/perm_templates/subject_list.txt | parallel mkdir -p ${SmoothDIR}/FWHM${SmoothFWHM}/{}
cd ${SmoothDIR}

## submit the smoothing jobs
qbatch --walltime 0:30:00 -c 30 -j 1 --ppj 8 -N smooth${SmoothFWHM} ~/myscripts/corr/pint/perm_templates/smoothing_jobslist.txt
```

## writing the PINT running step

we will run this in the similar fashion, although more environment variables are required

These jobs will require two enviromnent variables:

+ `${SmoothFWHM}` the Smoothing FWHM from the file/directory names
+ `${OUTPUTBASE}` the base directory for all the pint outputs
   + this is ${SCRATCH}/CORR/ + something specific to this run ${pint_version}
      + today the ${pint_version} is fix_v1.0 because it is using the fix cleaned derivatives and is my first run (v1.0)
+ `${CIFTIFY_TEMPLATES}` is the location of ciftify data --> where the PINT network input vertices are

**Note: it does appear that -j 2 is fine for PINT**

```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=4
pint_version="fix_v1.0" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for searchrad in 4 6 8 10 12; do
 for padratio in 1 1.5 2 2.5 3; do
   padrad=`echo "${searchrad} * ${padratio}" | bc -l`
   for subid in `cat ~/myscripts/corr/pint/perm_templates/subject_list.txt`; do
    mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/search_${searchrad}mm/pad_${padrad}mm/PINT_out/${subid}
   done
  done
done 
cd ${OUTPUTBASE}

## submit the smoothing jobs
qbatch --walltime 3:00:00 -c 12 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM} ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt
```

Soo.. turns out I can run them all at once because of my SciNet number of jobs quota..

So I ran this for Smooth4 to get it to run in chunks..

```sh
tail -n 1000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt |  qbatch --walltime 3:00:00 -c 12 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_01 -
tail -n 1100 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 100 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_01 -
tail -n 1700 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 600 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_03 -
tail -n 2000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_04 -
tail -n 2300 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_05 -
tail -n 2600 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_06 -
tail -n 2900 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_07 -
tail -n 3200 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_08 -
tail -n 3500 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_09 -
tail -n 3800 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_10 -
tail -n 4100 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_11 -
tail -n 4400 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_12 -
tail -n 4700 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_13 -
tail -n 5000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_14 -
tail -n 5300 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_15 -
tail -n 5600 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_16 -

...

 1046  tail -n 10100 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_31 -
 1047  tail -n 10400 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_32 -
 1048  tail -n 10700 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_33 -
 1049  tail -n 11000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_34 -
 1050  tail -n 11300 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_35 -
 1051  tail -n 11600 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_36 -
 1052  tail -n 11900 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_37 -
 1053  tail -n 12200 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_38 -
 1054  tail -n 12500 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_39 -
 1055  tail -n 12800 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_40 -
 1056  wc ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt 
 1057  tail -n 13100 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_41 -
 1058  tail -n 13400 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_42 -
 1059  head -n 176 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_43 -
 1060

```

Conclusion..I get at least 2000 jobs at a time??..i dunno why I couldn't submit the whole thing...

## 2017-10-16

Gonna try to run the 8mm first 8000 runs...
```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=8
pint_version="fix_v1.0" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for searchrad in 4 6 8 10 12; do
 for padratio in 1 1.5 2 2.5 3; do
   padrad=`echo "${searchrad} * ${padratio}" | bc -l`
   for subid in `cat ~/myscripts/corr/pint/perm_templates/subject_list.txt`; do
    mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/search_${searchrad}mm/pad_${padrad}mm/PINT_out/${subid}
   done
  done
done 
cd ${OUTPUTBASE}

## submit the smoothing jobs

```

head -n 2000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_01 -
head -n 4000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 2000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_02 -
head -n 5000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_03 -
head -n 6000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_04 -
head -n 7000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_05 -
head -n 8000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_06 -
head -n 9000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_07 -
...
head -n 12000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_10 -
tail -n 1576 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_11 -
```
# 2017-10-20

Gonna try to submit the 12 FWHM too

```
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=12
pint_version="fix_v1.0" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for searchrad in 4 6 8 10 12; do
 for padratio in 1 1.5 2 2.5 3; do
   padrad=`echo "${searchrad} * ${padratio}" | bc -l`
   for subid in `cat ~/myscripts/corr/pint/perm_templates/subject_list.txt`; do
    mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/search_${searchrad}mm/pad_${padrad}mm/PINT_out/${subid}
   done
  done
done 
cd ${OUTPUTBASE}

head -n 2000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_01 -
head -n 3000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_02 -
head -n 4000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_03 -
```
head -n 5000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pintfix_v1.012_04 -
head -n 7000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 2000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pintfix_v1.012_04 -
head -n 8000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_06 -
head -n 9000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_07 -
gpc-f103n084-ib0-$ head -n 10000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_08 -
45728878[].gpc-sched-ib0
gpc-f103n084-ib0-$ head -n 11000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_09 -
45728879[].gpc-sched-ib0
gpc-f103n084-ib0-$ head -n 12000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_10 -
45728880[].gpc-sched-ib0
gpc-f103n084-ib0-$ tail -n 1576 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_11 -
45728885[].gpc-sched-ib0

