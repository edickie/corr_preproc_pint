## 2018-01-24

## Running the two time consuming steps of the postPINT on the scc

```sh
module load /KIMEL/quarantine/modules/quarantine
module load Freesurfer/6.0.0
module load FSL/5.0.9-ewd
module load connectome-workbench/1.2.3
module load python/3.6_ciftify_01
module load GNU_PARALLEL/20170122



SCRIPTS_DIR=/KIMEL/tigrlab/projects/edickie/code/CORR/pint
for SmoothFWHM in 4 8 12; do
for searchrad in 4 6 8 10 12; do
 for padratio in 1 2 3; do
   padrad=`echo "${searchrad} * ${padratio}" | bc -l`
   WORKING_DIR=/KIMEL/tigrlab/scratch/edickie/CORR/PINT_fix_v1.0/FWHM${SmoothFWHM}/search_${searchrad}mm/pad_${padrad}mm
   echo "${SCRIPTS_DIR}/run_postPINT_calc.sh ${WORKING_DIR} btwsess_NYU_2 ${SCRIPTS_DIR}"  |\
      qbatch --walltime 2:00:00 -c 1 -j 1 --ppj 4 -N  postpint_s${SmoothFWHM}-${searchrad}-${padrad}_03 -
   echo "${SCRIPTS_DIR}/run_postPINT_calc.sh ${WORKING_DIR} btwsess_all ${SCRIPTS_DIR}"  |\
     qbatch --walltime 3:00:00 -c 1 -j 1 --ppj 4 -N  postpint_s${SmoothFWHM}-${searchrad}-${padrad}_04 -
   done
 done
done
```
