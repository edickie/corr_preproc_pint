#!/bin/bash

set -x

## read in the directory path to tar as argument 1
SITE=${1}

HCP_DATA=${SCRATCH}/CORR/hcp_fs6/${SITE}
EPIDIR=${SCRATCH}/CORR/epi_201704/${SITE}

## cd to the directory
cd ${EPIDIR}

## using the ABIDE subject prefix as what we want


## cp the qc data to the hcpdir
EPIQCCOPY=${HCP_DATA}/epi_201704_QC
mkdir ${EPIQCCOPY}
cp ${EPIDIR}/qc_icafix_*.tar  ${EPIQCCOPY}/
cp ${EPIDIR}/ica_fix_report_*.csv ${EPIQCCOPY}/
cp ${EPIDIR}/qc_reg_EPI_to_T1_RST.pdf ${EPIQCCOPY}/

## now do the shiz
for sess in 1 2 3; do
  for run in 1 2 3 4; do
  
  SUBJECTS=`ls -1d 002????s${sess}r${run}`
  for SUB in ${SUBJECTS}; do
  
  HCPsub=`cat ${EPIDIR}/${SUB}/T1/SESS01/hcp_subjID`
  NameOffMRI="sess${sess}rest${run}_fix"
  ## copy the params and the native space preprocessed image to the hcp directory
  PARAMSDIR="${HCP_DATA}/${HCPsub}/MNINonLinear/Results/${NameOffMRI}/PARAMS"
  NATIVEDIR="${HCP_DATA}/${HCPsub}/MNINonLinear/Results/${NameOffMRI}/native"
  mkdir ${NATIVEDIR}/
  cp ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_brain.nii.gz ${NATIVEDIR}/
  cp ${EPIDIR}/${SUB}/RST/SESS01/func_fix.20160422.01.nii.gz ${NATIVEDIR}/
  mkdir ${PARAMSDIR}/
  cp ${EPIDIR}/${SUB}/RST/SESS01/PARAMS/* ${PARAMSDIR}/

  ## rm the files we don't really need to back-up
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_mask.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_tmp_mean.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_tmp_vol.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_ts_mean.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_del.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_deskull.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_despike.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_detrend.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_mean.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_motion.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_scaled.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tmp_RAI.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tmp_detrend.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tmp_mean.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tshift.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/mat*.mat
  rm ${EPIDIR}/${SUB}/RST/SESS01/reg*.nii.gz
  rm -r ${EPIDIR}/${SUB}/RST/SESS01/MELODIC*
  rm ${EPIDIR}/${SUB}/T1/SESS01/*.nii.gz
  rm ${EPIDIR}/${SUB}/T1/SESS01/*.mat

done
done
done
