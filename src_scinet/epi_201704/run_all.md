## staging and running epitome

```sh
#!/bin/bash

## set the site variable here
SITE='UPSM'

SUBJECTLIST=${HOME}/myscripts/corr/sublists/${SITE}_anat_qced_list.csv


#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/epitome_env_nodatapaths.sh
module load qbatch
export EPITOME_DATA=${SCRATCH}/CORR/epi_201704
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout6/${SITE}
export HCP_DATA=${SCRATCH}/CORR/hcp_fs6/${SITE}

NII_DATA=${SCRATCH}/CORR/inputs/${SITE}

## stage the data
python ${HOME}/myscripts/corr/bin/stage_epitome_CORR.py \
  ${SUBJECTLIST} ${NII_DATA} ${HCP_DATA} ${EPITOME_DATA}/${SITE}

cd ${EPITOME_DATA}/${SITE}

bash master*.sh

## qbatch the magicks
echo "Submitting all subjects jobs"
cat proc*.sh | grep cmd | sed 's/bash //g' | \
  qbatch --walltime 1:30:00 -c 4 -j 4 --ppj 8 -N epi${SITE} -
```

## 2017-04-28

```sh
SITE=UPSM

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/ciftify_env_nodatapaths.sh
module load qbatch
export EPITOME_DATA=${SCRATCH}/CORR/epi_201704
export HCP_DATA=${SCRATCH}/CORR/hcp_fs6/${SITE}

cd ${EPITOME_DATA}/${SITE}


for SUBJECT in `ls -1d 002????s1r1`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess1rest1_fix 2 
done > func2hcpjoblist

for SUBJECT in `ls -1d 002????s2r1`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess2rest1_fix 2 
done >> func2hcpjoblist

for SUBJECT in `ls -1d 002????s3r1`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess3rest1_fix 2 
done >> func2hcpjoblist

qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N func2hcp${SITE} func2hcpjoblist
```

```sh
SITE=Utah

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/ciftify_env_nodatapaths.sh
module load qbatch
export EPITOME_DATA=${SCRATCH}/CORR/epi_201704
export HCP_DATA=${SCRATCH}/CORR/hcp_fs6/${SITE}

cd ${EPITOME_DATA}/${SITE}


for SUBJECT in `ls -1d 002????s1r1`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess1rest1_fix 2 
done > func2hcpjoblist

for SUBJECT in `ls -1d 002????s2r1`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess2rest1_fix 2 
done >> func2hcpjoblist

for SUBJECT in `ls -1d 002????s2r2`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess2rest2_fix 2 
done >> func2hcpjoblist

qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N func2hcp${SITE} func2hcpjoblist
```

## LMU_2 has one session with 4 runs..

```sh
SITE=LMU_2

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/ciftify_env_nodatapaths.sh
module load qbatch
export EPITOME_DATA=${SCRATCH}/CORR/epi_201704
export HCP_DATA=${SCRATCH}/CORR/hcp_fs6/${SITE}

cd ${EPITOME_DATA}/${SITE}


for SUBJECT in `ls -1d 002????s1r1`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess1rest1_fix 2 
done > func2hcpjoblist

for SUBJECT in `ls -1d 002????s1r2`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess1rest2_fix 2 
done >> func2hcpjoblist

for SUBJECT in `ls -1d 002????s1r3`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess1rest3_fix 2 
done >> func2hcpjoblist

for SUBJECT in `ls -1d 002????s1r4`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess1rest4_fix 2 
done >> func2hcpjoblist

qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N func2hcp${SITE} func2hcpjoblist
```

## NYU_2 I couldn't run untill I had archived the other one's..but the same procedure applies

```sh
SITE=NYU_2

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/ciftify_env_nodatapaths.sh
module load qbatch
export EPITOME_DATA=${SCRATCH}/CORR/epi_201704
export HCP_DATA=${SCRATCH}/CORR/hcp_fs6/${SITE}

cd ${EPITOME_DATA}/${SITE}


for SUBJECT in `ls -1d 002????s1r1`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess1rest1_fix 2 
done > func2hcpjoblist

for SUBJECT in `ls -1d 002????s1r2`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess1rest2_fix 2 
done >> func2hcpjoblist

for SUBJECT in `ls -1d 002????s2r1`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess2rest1_fix 2 
done >> func2hcpjoblist

for SUBJECT in `ls -1d 002????s2r2`; do
  HCPDEST=$(cat ${EPITOME_DATA}/${SITE}/${SUBJECT}/T1/SESS01/hcp_subjID)
  echo func2hcp.py --FLIRT-template ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/anat_EPI_brain.nii.gz ${EPITOME_DATA}/${SITE}/${SUBJECT}/RST/SESS01/func_fix.20160422.01.nii.gz $HCPDEST sess2rest2_fix 2 
done >> func2hcpjoblist

qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N func2hcp${SITE} func2hcpjoblist
```
## Running the epitome qc..

```sh
#!/bin/bash

## set the site variables, and training set
SITE='UPSM'
TrainingSet='CORR29UPSM'

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/epitome_env_nodatapaths.sh
module load qbatch
export EPITOME_DATA=${SCRATCH}/CORR/epi_201704
cd ${EPITOME_DATA}/${SITE}
echo "Submitting the epi-qc job"
echo ${HOME}/myscripts/corr/fix_pretrain/epi-qc-CORR.sh ${EPITOME_DATA} ${SITE} proc*.sh ${TrainingSet} | \
  qbatch --walltime 3:00:00 -c 1 -j 1 --ppj 8 -N epiqc${SITE} -
```

for Utah..just change the top of the above section to 
SITE=Utah
TrainingSet=CORR26Utah

for LMU_2..change the top to..
SITE=LMU_2
TrainingSet=CORR24LMU2

## Sending the HCP qc 
```sh
#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/ciftify_env_nodatapaths.sh
module load qbatch
module load ImageMagick

for SITE in "LMU_2" "Utah" "UPSM"; do
echo /home/a/arisvoin/edickie/myscripts/corr/epi_201704/qc_func2cifti_CORR.sh ${SITE}\
 | qbatch --walltime 5:30:00 -c 1 -j 1 --ppj 8 -N qcfunc2hcp${SITE} -

echo /home/a/arisvoin/edickie/myscripts/corr/epi_201704/qc_RSNviews_CORR.sh ${SITE}\
 | qbatch --walltime 12:00:00 -c 1 -j 1 --ppj 8 -N qcRSN${SITE} -
done
```
## Sending the HCP qc 
```sh
#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/ciftify_env_nodatapaths.sh
module load qbatch
module load ImageMagick

SITE=NYU_2
echo /home/a/arisvoin/edickie/myscripts/corr/epi_201704/qc_func2cifti_CORR.sh ${SITE}\
 | qbatch --walltime 12:00:00 -c 1 -j 1 --ppj 8 -N qcfunc2hcp${SITE} -

echo /home/a/arisvoin/edickie/myscripts/corr/epi_201704/qc_RSNviews_CORR.sh ${SITE}\
 | qbatch --walltime 24:00:00 -c 1 -j 1 --ppj 8 -N qcRSN${SITE} -

```

# 2017-05-20  Smoothing add additional smoothing

```sh
#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/ciftify_env_nodatapaths.sh
module load qbatch

OriginalSmoothingFWHM="2"
FinalSmoothingFWHM="8"

AdditionalSmoothingFWHM=`echo "sqrt(( $FinalSmoothingFWHM ^ 2 ) - ( $OriginalSmoothingFWHM ^ 2 ))" | bc -l`

AdditionalSigma=`echo "$AdditionalSmoothingFWHM / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`

HCP_BASE=${SCRATCH}/CORR/hcp_fs6/

cd ${HCP_BASE}
for site in UPSM Utah LMU_2 NYU_2; do
  HCP_DATA=${HCP_BASE}/${site}
  cd ${HCP_DATA}
  for dtseriesfile in `ls */MNINonLinear/Results/*/*s${OriginalSmoothingFWHM}.dtseries.nii`; do
    subid=$(basename $(dirname $(dirname $(dirname $(dirname $dtseriesfile)))))
    nameoffmri=$(basename $(dirname ${dtseriesfile}))
    echo wb_command -cifti-smoothing\
      ${HCP_DATA}/${dtseriesfile} \
      ${AdditionalSigma} ${AdditionalSigma} COLUMN\
      ${HCP_DATA}/${subid}/MNINonLinear/Results/${nameoffmri}/${nameoffmri}_Atlas_s${FinalSmoothingFWHM}.dtseries.nii \
      -left-surface ${HCP_DATA}/${subid}/MNINonLinear/fsaverage_LR32k/${subid}.L.midthickness.32k_fs_LR.surf.gii \
      -right-surface ${HCP_DATA}/${subid}/MNINonLinear/fsaverage_LR32k/${subid}.R.midthickness.32k_fs_LR.surf.gii
  done | qbatch --walltime 00:30:00 -c 16 -j 16 --ppj 8 -N sm${FinalSmoothingFWHM}${site} -
done

# qbatch --walltime 00:30:00 -c 16 -j 16 --ppj 8 -N sm${FinalSmoothingFWHM}${SITE} -
```

## 2017-07-07 Need to rurun all the projections

```sh

cd ${HCP_DATA}
for niifile in 00*/MNINonLinear/Results/sess?rest?_acompcor/native/*lowpass_rest0?.nii.gz; do
  echo ciftify_subject_fmri ${niifile} \
	$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${niifile})))))) \
	$(basename $(dirname $(dirname ${niifile}))) 
done


```
