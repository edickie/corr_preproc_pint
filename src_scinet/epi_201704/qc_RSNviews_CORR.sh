#!/bin/bash
#PBS -l nodes=1:ppn=8,walltime=3:00:00
#PBS -j oe

## set the site variable here
SITE=${1}

export HCP_DATA=${SCRATCH}/CORR/hcp_fs6/${SITE}

tmpdir=$(mktemp --tmpdir=/dev/shm -d tmp.XXXXXX)

function cleanup_ramdisk {
    echo -n "Cleaning up ramdisk directory ${tmpdir} on "
    date
    rm -rf ${tmpdir}
    echo -n "done at "
    date
}

#trap the termination signal, and call the function 'trap_term' when
# that happens, so results may be saved.
trap cleanup_ramdisk EXIT

## get the subjects list from the HCP_DATA list
cd ${HCP_DATA}

## figure out the type of Results to QC
ResultTypes=$(for i in `ls -1d */MNINonLinear/Results/*`; do echo $(basename $i); done | sort | uniq)

## now for the RSN-viewer
qcdir="qc_${SITE}_RSNviews"
for NameOffMRI in ${ResultTypes}; do
for file in */MNINonLinear/Results/${NameOffMRI}/*dtseries.nii; do 
 echo cifti_vis_RSN cifti-snaps --qcdir ${tmpdir}/${qcdir} ${file} $(dirname $(dirname $(dirname $(dirname $file)))) 
done | parallel -j 4 
done

cifti_vis_RSN index --qcdir ${tmpdir}/${qcdir}

## move the data from the ramdisk back to HCP_DATA
cd ${tmpdir}
tar -cf ${HCP_DATA}/${qcdir}.tar ${qcdir}/
rm -r ${tmpdir}/${qcdir}
cd ${HCP_DATA}
