## first steps to staging epitome for fix training

```sh
#!/bin/bash

## set the site variable here
SITE='LMU_2'

SUBJECTLIST=${HOME}/myscripts/corr/sublists/${SITE}_fix_training_list.csv
MASTERSCRIPT=master_160319_intfix_20160422.sh
PROCLIST=proclist_160319_intfix_20160422.sh


#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/epitome_env_nodatapaths.sh
module load qbatch
export EPITOME_DATA=${SCRATCH}/CORR/epi-fixtrain
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout6/${SITE}
export HCP_DATA=${SCRATCH}/CORR/hcp_fs6/${SITE}

NII_DATA=${SCRATCH}/CORR/inputs/${SITE}

## stage the data
python ${HOME}/myscripts/corr/bin/stage_epitome_CORR.py \
  ${SUBJECTLIST} ${NII_DATA} ${HCP_DATA} ${EPITOME_DATA}/${SITE}

cd ${EPITOME_DATA}/${SITE}

bash ${MASTERSCRIPT}

## qbatch the magicks
echo "Submitting all subjects jobs"
cat ${PROCLIST} | grep cmd | sed 's/bash //g' | \
  qbatch --walltime 1:30:00 -c 4 -j 4 --ppj 8 -N epi${SITE} -

```

## 2017-04-18

```sh
#!/bin/bash

## set the site variable here
SITE='LMU_2'

SUBJECTLIST=${HOME}/myscripts/corr/sublists/${SITE}_fix_training_list.csv
MASTERSCRIPT=master_160319_intfix_20160422.sh
PROCLIST=proclist_160319_intfix_20160422.sh


#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/epitome_env_nodatapaths.sh
module load qbatch
export EPITOME_DATA=${SCRATCH}/CORR/epi-fixtrain
cd ${EPITOME_DATA}/${SITE}
echo "Submitting the epi-qc job"
echo ${HOME}/myscripts/corr/fix_pretrain/epi-qc-CORR.sh ${EPITOME_DATA} ${SITE} ${PROCLIST} Standard | \
  qbatch --walltime 0:45:00 -c 1 -j 1 --ppj 8 -N epiqc${SITE} -
```
## 2017-04-26

training the data.. to do so we qsubed the training scripts
```sh
qsub trainfix_LMU_2.sh
qsub trainfix_NYU_2.sh
qsub trainfix_UPSM.sh
qsub trainfix_Utah.sh			
```

## 2017-04-27

after fix traning we noticed none of the TPN values were good for our classifiers..so we are going to re-train them..

My plan it to re-build a fix qc pages using the training set as our new baseline..then reajust only those I really disagree with..


