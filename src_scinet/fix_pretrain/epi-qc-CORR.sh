#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=2:00:00
#PBS -j oe

set -x

EPITOME_DATA=${1}
SITE=${2}
proclist=${3}
FIXTrainingSet=${4}

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
# source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${EPITOME_DATA}

epidir=${EPITOME_DATA}/${SITE}
qcdir="qc_icafix_${SITE}"

tmpdir=$(mktemp --tmpdir=/dev/shm -d tmp.XXXXXX)

function cleanup_ramdisk {
    echo -n "Cleaning up ramdisk directory ${tmpdir} on "
    date
    rm -rf ${tmpdir}
    echo -n "done at "
    date
}

#trap the termination signal, and call the function 'trap_term' when
# that happens, so results may be saved.
trap cleanup_ramdisk EXIT

### run ica_fix qc and copy all the qc to ramdisk then tar it up
cd ${epidir}
epi-qc-icafix --debug --copy-qcdir ${tmpdir}/${qcdir} --labelfilename fix4melview_${FIXTrainingSet}_thr20.txt */RST/SESS01/fake*feat/
cd ${tmpdir}
tar -cf ${epidir}/${qcdir}.tar ${qcdir}/
rm -r ${tmpdir}/${qcdir}

## generate the other qc pdfs
cd ${epidir}
cat ${proclist} | grep qc | parallel -j 4
