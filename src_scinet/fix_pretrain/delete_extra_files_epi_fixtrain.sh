#!/bin/bash

set -x

## read in the directory path to tar as argument 1
SITE=${1}

EPIDIR=${SCRATCH}/CORR/epi-fixtrain/${SITE}

## cd to the directory
cd ${EPIDIR}

## using the ABIDE subject prefix as what we want
SUBJECTS=`ls -1d 002????s?r?`


## now do the shiz
for SUB in ${SUBJECTS}; do

  ## rm the files we don't really need to back-up
  rm ${EPIDIR}/${SUB}/RST/SESS01/PARAMS/3dvolreg.20160422.01.aff12.1D
  rm ${EPIDIR}/${SUB}/RST/SESS01/PARAMS/dif.motion.20160422.01.1D
  rm ${EPIDIR}/${SUB}/RST/SESS01/PARAMS/lag.motion.20160422.01.1D
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat*.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_del.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_deskull.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_despike.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_detrend.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_mean.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_motion.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_scaled.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tmp_RAI.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tmp_detrend.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tmp_mean.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tshift.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/mat*.mat
  rm ${EPIDIR}/${SUB}/RST/SESS01/reg*.nii.gz
  rm -r ${EPIDIR}/${SUB}/RST/SESS01/MELODIC*
  rm ${EPIDIR}/${SUB}/T1/SESS01/*.nii.gz
  rm ${EPIDIR}/${SUB}/T1/SESS01/*.mat

done
