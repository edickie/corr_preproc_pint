#!/bin/bash
# run_postPINT_calc.sh

# Runs three scripts on a PINT output to do test restest calculations
#
# Usage:
# run_postPINT_calc.sh <WORKING_DIR> <ANA_PREFIX> <SCRIPTS_DIR>
#
# Arguments:
#  <WORKING_DIR>  Directory that will hold the PINT outputs and the result (one above PINT_out)
#  <ANA_PREFIX>   A string prefix that should correspond to a _list.csv and _txtkey.csv file
#  <SCRIPTS_DIR>  The directory holding the perm_templates folder and the testretest_calc.R script


WORKING_DIR=${1}
ANA_PREFIX=${2}
SCRIPTS_DIR=${3}

mkdir ${WORKING_DIR}/${ANA_PREFIX}
cd ${WORKING_DIR}/${ANA_PREFIX}

ciftify_postPINT1_concat \
  ${ANA_PREFIX}_concat.csv \
  `cat ${SCRIPTS_DIR}/perm_templates/${ANA_PREFIX}_list.csv`

ciftify_postPINT2_sub2sub \
  ${ANA_PREFIX}_concat.csv \
  ${ANA_PREFIX}_sub2sub.csv

# ${SCRIPTS_DIR}/testretest_calc.R \
#   ${WORKING_DIR}/PINT_out/ \
#   ${SCRIPTS_DIR}/perm_templates/${ANA_PREFIX}_trtkey.csv \
#   ${ANA_PREFIX}_sub2sub.csv \
#   ${WORKING_DIR}/${ANA_PREFIX}
