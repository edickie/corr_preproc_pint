#!/bin/bash

PINT_OUTPUTS=${SCRATCH}/CORR/PINT_fix_v1.2

logs_dir=${PINT_OUTPUTS}/logs
qbatch_dir=${PINT_OUTPUTS}/.qbatch
queued_list=${qbatch_dir}/tmp_queued

if [ -f ${queued_list} ]; then
  rm ${queued_list}
fi

qstat -f | grep Job_Name | cut -d . -f 2 > ${queued_list}
cd ${logs_dir} ; ls | sort | cut -d . -f 2 | uniq >> ${queued_list}


cd ${qbatch_dir}
not_submitted=''
for qbatch_job in `ls | grep -v tmp_queued | cut -d . -f 2`; do
  is_done=`grep ${qbatch_job} ${queued_list} | wc -l`
  if [ ${is_done} -lt 1 ] ; then
    echo qsub pintfix_v1.${qbatch_job}.array
  fi
done

rm ${queued_list}
