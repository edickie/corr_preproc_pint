# running PINT permutations

template files for these scripts were created in R and are in perm_templates folder.

## 2017-10-15 The smoothing step

To be careful about env variable for the smoothing kernal, each one was launched from a different gpc node

This step is repeated with smoothing kernels 4 6 8 10 12

**NOTE: today we learned that wb_command -cifti-smoothing need the whole GPC node to run effectively... (i.e. use -j 1)**

```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422
export SmoothFWHM=12
export SmoothSigma=`echo "${SmoothFWHM} / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`

## create the directory structure
SmoothDIR=${SCRATCH}/CORR/temp_smooth
cat ~/myscripts/corr/pint/perm_templates/subject_list.txt | parallel mkdir -p ${SmoothDIR}/FWHM${SmoothFWHM}/{}
cd ${SmoothDIR}

## submit the smoothing jobs
qbatch --walltime 0:30:00 -c 30 -j 1 --ppj 8 -N smooth${SmoothFWHM} ~/myscripts/corr/pint/perm_templates/smoothing_jobslist.txt
```

## writing the PINT running step

we will run this in the similar fashion, although more environment variables are required

These jobs will require two enviromnent variables:

+ `${SmoothFWHM}` the Smoothing FWHM from the file/directory names
+ `${OUTPUTBASE}` the base directory for all the pint outputs
   + this is ${SCRATCH}/CORR/ + something specific to this run ${pint_version}
      + today the ${pint_version} is fix_v1.0 because it is using the fix cleaned derivatives and is my first run (v1.0)
+ `${CIFTIFY_TEMPLATES}` is the location of ciftify data --> where the PINT network input vertices are

**Note: it does appear that -j 2 is fine for PINT**

```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=4
pint_version="fix_v1.0" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for searchrad in 4 6 8 10 12; do
 for padratio in 1 1.5 2 2.5 3; do
   padrad=`echo "${searchrad} * ${padratio}" | bc -l`
   for subid in `cat ~/myscripts/corr/pint/perm_templates/subject_list.txt`; do
    mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/search_${searchrad}mm/pad_${padrad}mm/PINT_out/${subid}
   done
  done
done
cd ${OUTPUTBASE}

## submit the smoothing jobs
qbatch --walltime 3:00:00 -c 12 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM} ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt
```

Soo.. turns out I can run them all at once because of my SciNet number of jobs quota..

So I ran this for Smooth4 to get it to run in chunks..

```sh
tail -n 1000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt |  qbatch --walltime 3:00:00 -c 12 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_01 -
tail -n 1100 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 100 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_01 -
tail -n 1700 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 600 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_03 -
tail -n 2000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_04 -
tail -n 2300 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_05 -
tail -n 2600 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_06 -
tail -n 2900 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_07 -
tail -n 3200 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_08 -
tail -n 3500 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_09 -
tail -n 3800 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_10 -
tail -n 4100 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_11 -
tail -n 4400 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_12 -
tail -n 4700 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_13 -
tail -n 5000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_14 -
tail -n 5300 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_15 -
tail -n 5600 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_16 -

...

 1046  tail -n 10100 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_31 -
 1047  tail -n 10400 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_32 -
 1048  tail -n 10700 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_33 -
 1049  tail -n 11000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_34 -
 1050  tail -n 11300 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_35 -
 1051  tail -n 11600 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_36 -
 1052  tail -n 11900 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_37 -
 1053  tail -n 12200 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_38 -
 1054  tail -n 12500 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_39 -
 1055  tail -n 12800 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_40 -
 1056  wc ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt
 1057  tail -n 13100 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_41 -
 1058  tail -n 13400 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | head -n 300 | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_42 -
 1059  head -n 176 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | qbatch --walltime 2:00:00 -c 6 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_43 -
 1060

```

Conclusion..I get at least 2000 jobs at a time??..i dunno why I couldn't submit the whole thing...

## 2017-10-16

Gonna try to run the 8mm first 8000 runs...
```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=8
pint_version="fix_v1.0" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for searchrad in 4 6 8 10 12; do
 for padratio in 1 1.5 2 2.5 3; do
   padrad=`echo "${searchrad} * ${padratio}" | bc -l`
   for subid in `cat ~/myscripts/corr/pint/perm_templates/subject_list.txt`; do
    mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/search_${searchrad}mm/pad_${padrad}mm/PINT_out/${subid}
   done
  done
done
cd ${OUTPUTBASE}

## submit the smoothing jobs

```

head -n 2000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_01 -
head -n 4000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 2000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_02 -
head -n 5000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_03 -
head -n 6000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_04 -
head -n 7000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_05 -
head -n 8000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_06 -
head -n 9000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_07 -
...
head -n 12000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_10 -
tail -n 1576 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_11 -
```
# 2017-10-20

Gonna try to submit the 12 FWHM too

```
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=12
pint_version="fix_v1.0" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for searchrad in 4 6 8 10 12; do
 for padratio in 1 1.5 2 2.5 3; do
   padrad=`echo "${searchrad} * ${padratio}" | bc -l`
   for subid in `cat ~/myscripts/corr/pint/perm_templates/subject_list.txt`; do
    mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/search_${searchrad}mm/pad_${padrad}mm/PINT_out/${subid}
   done
  done
done
cd ${OUTPUTBASE}
```
head -n 2000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_01 -
head -n 3000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_02 -
head -n 4000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_03 -
```
head -n 5000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pintfix_v1.012_04 -
head -n 7000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 2000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pintfix_v1.012_04 -
head -n 8000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_06 -
head -n 9000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_07 -
gpc-f103n084-ib0-$ head -n 10000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_08 -
45728878[].gpc-sched-ib0
gpc-f103n084-ib0-$ head -n 11000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_09 -
45728879[].gpc-sched-ib0
gpc-f103n084-ib0-$ head -n 12000 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | tail -n 1000 | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_10 -
45728880[].gpc-sched-ib0
gpc-f103n084-ib0-$ tail -n 1576 ~/myscripts/corr/pint/perm_templates/pint_jobslist.txt | qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}${SmoothFWHM}_11 -
45728885[].gpc-sched-ib0

## 2018-01-04 We deleted the old smoothing temp files..so we will rerun those

NB. rerun this for all smoothing kernels

```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

export SmoothFWHM=2
export SmoothSigma=`echo "${SmoothFWHM} / ( 2 * ( sqrt ( 2 * l ( 2 ) ) ) )" | bc -l`

## create the directory structure
SmoothDIR=${SCRATCH}/CORR/temp_smooth
cat ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/subject_list.txt | parallel mkdir -p ${SmoothDIR}/FWHM${SmoothFWHM}/{}
cd ${SmoothDIR}

## submit the smoothing jobs
qbatch --walltime 0:30:00 -c 30 -j 1 --ppj 8 -N smooth${SmoothFWHM} ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/smoothing_jobslist_NYUUtah.txt
```

## Now for the running of pint

The decided params:
+ smoothing 2 4 6 8 10
+ padding 8 10 12 14
+ search 4 6 8 10
+ sampling  4 6 8 10

```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=4
pint_version="fix_v1.2" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for samprad in 4 6 8 10; do
  for searchrad in 4 6 8 10; do
   for padrad in 8 10 12 14; do
     export samprad
     export searchrad
     export padrad
     for subid in `cat ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/subject_list.txt`; do
      mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/${subid}
     done
     cd ${OUTPUTBASE}
     ## submit the smoothing jobs
     qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}sm${SmoothFWHM}sa${samprad}sc${searchrad}pd${padrad} ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/pint_jobslist_byparam_NYUUtah_wtnsess.txt
    done
  done
done
```

# 2017-01-10 look for completed

So the scinet queue was still mean..looks like some jobs got cancelled..long story

Need to run this loop to report how many finished

```sh
SmoothFWHM=4
pint_version="fix_v1.2" #assuming I will probably rerun this..
OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for samprad in 4 6 8 10; do
  for searchrad in 4 6 8 10; do
   for padrad in 8 10 12 14; do
      num_done=`ls -1d ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*_summary.csv | wc -l`
      if [ ${num_done} -lt 316 ]; then
      echo ${num_done}  pint${pint_version}sm${SmoothFWHM}sa${samprad}sc${searchrad}pd${padrad}.array
      rm ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*_summary.csv
      rm ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*log
      rm ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*_meants.csv
      rm ${OUTPUTBASE}/logs/pint${pint_version}sm${SmoothFWHM}sa${samprad}sc${searchrad}pd${padrad}*
      fi
    done
  done
done
```


300 pintfix_v1.2sm4sa6sc8pd8.array
276 pintfix_v1.2sm4sa6sc8pd12.array
54 pintfix_v1.2sm4sa8sc4pd10.array
232 pintfix_v1.2sm4sa8sc6pd8.array
238 pintfix_v1.2sm4sa8sc6pd10.array
160 pintfix_v1.2sm4sa8sc10pd10.array
256 pintfix_v1.2sm4sa8sc10pd12.array



qsub pintfix_v1.2sm4sa8sc4pd10.array
qsub pintfix_v1.2sm4sa8sc6pd10.array
qsub pintfix_v1.2sm4sa8sc6pd8.array

# 2017-01-11 Now we will run the FWHM 8 version

```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=8
pint_version="fix_v1.2" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for samprad in 4 6 8 10; do
  for searchrad in 4 6 8 10; do
   for padrad in 8 10 12 14; do
     export samprad
     export searchrad
     export padrad
     for subid in `cat ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/subject_list.txt`; do
      mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/${subid}
     done
     cd ${OUTPUTBASE}
     ## submit the smoothing jobs
     qbatch -n --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}sm${SmoothFWHM}sa${samprad}sc${searchrad}pd${padrad} ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/pint_jobslist_byparam_NYUUtah_wtnsess.txt
    done
  done
done
```

# 2018-01-15 we had some of the same problems with sm8 for we will repeat this process of finding the missing files

```sh
SmoothFWHM=4
pint_version="fix_v1.2" #assuming I will probably rerun this..
OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for samprad in 4 6 8 10; do
  for searchrad in 4 6 8 10; do
   for padrad in 8 10 12 14; do
      num_done=`ls -1d ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*_summary.csv | wc -l`
      if [ ${num_done} -lt 316 ]; then
       echo ${num_done}  pint${pint_version}sm${SmoothFWHM}sa${samprad}sc${searchrad}pd${padrad}.array
      # rm ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*_summary.csv
      # rm ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*log
      # rm ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*_meants.csv
      # rm ${OUTPUTBASE}/logs/pint${pint_version}sm${SmoothFWHM}sa${samprad}sc${searchrad}pd${padrad}*
      fi
    done
  done
done
```



294 pintfix_v1.2sm8sa6sc4pd12.array
76 pintfix_v1.2sm8sa6sc8pd10.array
314 pintfix_v1.2sm8sa8sc4pd12.array
314 pintfix_v1.2sm8sa8sc4pd14.array
314 pintfix_v1.2sm8sa8sc6pd14.array


```sh
SmoothFWHM=8
pint_version="fix_v1.2" #assuming I will probably rerun this..
OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
samprad="4"
searchrad="4"
padrad="10"
rm ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*_summary.csv
rm ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*log
rm ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/*/*_meants.csv
rm ${OUTPUTBASE}/logs/pint${pint_version}sm${SmoothFWHM}sa${samprad}sc${searchrad}pd${padrad}*
```

```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=8
pint_version="fix_v1.2" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for samprad in 4; do
  for searchrad in 4; do
   for padrad in 10; do
     export samprad
     export searchrad
     export padrad
     for subid in `cat ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/subject_list.txt`; do
      mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/${subid}
     done
     cd ${OUTPUTBASE}
     ## submit the smoothing jobs
     qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}sm${SmoothFWHM}sa${samprad}sc${searchrad}pd${padrad} ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/pint_jobslist_byparam_NYUUtah_wtnsess.txt
    done
  done
done
```

## Send the second bit

```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

pint_version="fix_v1.2" #assuming I will probably rerun this..
OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data
MYSCRIPTS=/home/a/arisvoin/edickie/myscripts/corr_preproc_pint/src_scinet/pint

## create the directory structure
cd ${OUTPUTBASE}

parallel "echo ${MYSCRIPTS}/run_postPINT_calc.sh  ${OUTPUTBASE}/FWHM{1}/samp_{2}/search_{3}/pad_{4} wthsess_NYU_2 ${MYSCRIPTS}" ::: 4 8 ::: 4 6 8 10 ::: 4 6 8 10 ::: 8 10 12 14 | qbatch --walltime 2:00:00 -c 2 -j 2 --ppj 8 -N postPINTnyu -

parallel "echo ${MYSCRIPTS}/run_postPINT_calc.sh  ${OUTPUTBASE}/FWHM{1}/samp_{2}/search_{3}/pad_{4} wthsess_Utah ${MYSCRIPTS}" ::: 4 8 ::: 4 6 8 10 ::: 4 6 8 10 ::: 8 10 12 14 | qbatch --walltime 0:30:00 -c 2 -j 2 --ppj 8 -N postPINTUtah -

```

# 2018-02-01 running PINT with special params

first we are going to link the unsmoothed files

```sh
## go to new gpc node
gpc

## set the env
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

export SmoothFWHM=0
export SmoothSigma=0

## create the directory structure
SmoothDIR=${SCRATCH}/CORR/temp_smooth
cat ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/subject_list.txt | parallel mkdir -p ${SmoothDIR}/FWHM${SmoothFWHM}/{}
cd ${SmoothDIR}

## submit the smoothing jobs
bash ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/cp_dtseries_sm0_NYUUtah.txt
```

## the things to submit

+ 6 6 12 with corr
+ presmooth 8,
    + 6 6 12
    + 8 6 12
    + 10 6 12
    + 8 6 8
    + 10 6 10


```sh
## go to new gpc node
gpc

## set the env
source ~/myscripts/corr_preproc_pint/src_scinet/ciftify_env_3.6.sh
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=8
pint_version="fix_corr" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for samprad in 6; do
  for searchrad in 6; do
   for padrad in 12; do
     export samprad
     export searchrad
     export padrad
     for subid in `cat ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/subject_list.txt`; do
      mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/${subid}
     done
     cd ${OUTPUTBASE}
     ## submit the smoothing jobs
     qbatch -n --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}sm${SmoothFWHM}sa${samprad}sc${searchrad}pd${padrad} ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/pint_jobslist_byparam_NYUUtah_wtnsess_corr.txt
    done
  done
done
```

+ presmooth 8,
    + 6 6 12
    + 8 6 12
    + 10 6 12
    + 8 6 8
    + 10 6 10

```sh
## go to new gpc node
gpc

## set the env
source ~/myscripts/corr_preproc_pint/src_scinet/ciftify_env_3.6.sh
module load gnu-parallel/20170422

## export run specific vars
export SmoothFWHM=0
export PreSmoothFWHM=8
pint_version="fix_presmooth8" #assuming I will probably rerun this..
export OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

## create the directory structure
for samprad in 6 8 10; do
  for searchrad in 6; do
   for padrad in 6 12; do
     export samprad
     export searchrad
     export padrad
     for subid in `cat ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/subject_list.txt`; do
      mkdir -p ${OUTPUTBASE}/FWHM${SmoothFWHM}/samp_${samprad}/search_${searchrad}/pad_${padrad}/PINT_out/${subid}
     done
     cd ${OUTPUTBASE}
     ## submit the smoothing jobs
     qbatch --walltime 0:40:00 -c 2 -j 2 --ppj 8 -N pint${pint_version}sm${SmoothFWHM}sa${samprad}sc${searchrad}pd${padrad} ~/myscripts/corr_preproc_pint/src_scinet/pint/perm_templates/pint_jobslist_byparam_NYUUtah_wtnsess_presmooth.txt
    done
  done
done
```

## rerunning the postPINT calcualations

```sh
## go to new gpc node
gpc

## set the env
source ~/myscripts/corr_preproc_pint/src_scinet/ciftify_env_3.6.sh
module load gnu-parallel/20170422


CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data
MYSCRIPTS=/home/a/arisvoin/edickie/myscripts/corr_preproc_pint/src_scinet/pint

## create the directory structure
pint_version="fix_corr"
OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
cd ${OUTPUTBASE}

echo "${MYSCRIPTS}/run_postPINT_calc.sh  ${OUTPUTBASE}/FWHM8/samp_6/search_6/pad_12 wthsess_NYU_2 ${MYSCRIPTS}" | qbatch --walltime 2:00:00 -c 2 -j 2 --ppj 8 -N postPINTnyu -

echo "${MYSCRIPTS}/run_postPINT_calc.sh  ${OUTPUTBASE}/FWHM8/samp_6/search_6/pad_12 wthsess_Utah ${MYSCRIPTS}" | qbatch --walltime 0:30:00 -c 2 -j 2 --ppj 8 -N postPINTUtah -

## run postPINT on stuff

pint_version="fix_presmooth8"
OUTPUTBASE=${SCRATCH}/CORR/PINT_${pint_version}
cd ${OUTPUTBASE}

parallel "echo ${MYSCRIPTS}/run_postPINT_calc.sh  ${OUTPUTBASE}/FWHM{1}/samp_{2}/search_{3}/pad_{4} wthsess_NYU_2 ${MYSCRIPTS}" ::: 0 ::: 6 8 10 ::: 6 ::: 6 12 | qbatch --walltime 2:00:00 -c 2 -j 2 --ppj 8 -N postPINTnyu -

parallel "echo ${MYSCRIPTS}/run_postPINT_calc.sh  ${OUTPUTBASE}/FWHM{1}/samp_{2}/search_{3}/pad_{4} wthsess_Utah ${MYSCRIPTS}" ::: 0 ::: 6 8 10 ::: 6 ::: 6 12 | qbatch --walltime 0:30:00 -c 2 -j 2 --ppj 8 -N postPINTUtah -

```
