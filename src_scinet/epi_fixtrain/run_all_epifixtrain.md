## first steps to staging epitome for fix training

```sh
#!/bin/bash

## set the site variable here
SITE='LMU_2'

SUBJECTLIST=${HOME}/myscripts/corr/sublists/${SITE}_fix_training_list.csv
MASTERSCRIPT=master_160422_${SITE}_20160422.sh
PROCLIST=proclist_160422_${SITE}_20160422.sh


#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/corr/epitome_env_nodatapaths.sh
module load qbatch
export EPITOME_DATA=${SCRATCH}/CORR/epi-fixtrain
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout/${SITE}
export HCP_DATA=${SCRATCH}/CORR/hcp/${SITE}

NII_DATA=${SCRATCH}/CORR/inputs/${SITE}

## stage the data
python ${HOME}/myscripts/corr/bin/stage_epitome_CORR.py \
  ${SUBJECTLIST} ${NII_DATA} ${HCP_DATA} ${EPITOME_DATA}/${SITE}

cd ${EPITOME_DATA}/${SITE}

bash ${MASTERSCRIPT}

## qbatch the magicks
echo "Submitting all subjects jobs"
cat ${PROCLIST} | grep cmd | sed 's/bash //g' | \
  qbatch --walltime 1:30:00 -c 4 -j 4 --ppj 8 -N epi${SITE} -

echo "Submitting the epi-qc job"
echo ${HOME}/myscripts/abide/epi-201604-qc.sh ${SITE} ${PROCLIST} ${FIXTRAININSET}| \
  qbatch --walltime 0:45:00 -c 1 -j 1 --ppj 8 -N epiqc${SITE} --afterok epi${SITE} -
```
