#!/usr/bin/env python
"""
Grabs the processed T1 from HCP folder as well as the functional data from the inputs.
Read a csv to get the mapping.

Usage:
    stage_epi_CORR [options] <subjectlist_csv> <nii_inputdir> <hcp_data_dir> <epi_data_dir> 

Arguements:
     <subjectlist_csv>      A csv holding mappings for each functional file
     <nii_inputdir>         Path to raw nifti inputs (for functional data)
     <hcp_data_dir>         Path to HCP_DATA (for preprocessed T1w data)
     <epi_data_dir>         Path to epitome data directory (will be created doesn't exist).

Options:
      --debug                  Debug logging in Erin's very verbose style
      -n,--dry-run             Dry run
      --help                   prints this message.

Details:
These are actually freesurfer outputs that have already been converted to nifty
and reoriented.  This to use as a standard anatomical this is
slow, but provides the highest-quality registration target. This also allows us
to take advantage of the high-quality freesurfer segmentations for nuisance time
series regression, if desired.

The subjectlist_csv contains three critical columns "anat_id", "rest_id", and "input_path".
The "anat_id" column holds the subject id for the data in the HCP_DATA folder.
The "rest_id" columns holds the subject id for the functonal run (will be the epitome subject id).
The "input_path" column holds the path (inside the "nii_inputdir") to the raw functional file.

Written by Erin W Dickie, April 16, 2017
"""

# In[28]:

import os, sys
import subprocess
import pandas as pd
from epitome.docopt import docopt


# In[29]:

def docmd(cmdlist):
    "sends a command (inputed as a list) to the shell"
    if DEBUG: print ' '.join(cmdlist)
    if not DRYRUN: subprocess.call(cmdlist)


# In[30]:

def export_from_hcp(hcp_subj_dir, epi_data_dir):
    ''' copies the T1w data from the hcp subjects dir to the epitome directory'''

    dir_i = os.path.join(hcp_subj_dir)
    dir_o = os.path.join(epi_data_dir)

    with open(os.path.join(dir_o, 'hcp_subjID'), 'w') as hcpnote:
        hcpnote.write('{}'.format(os.path.basename(hcp_subj_dir)))

    # convert freesurfer T1 to NII
    anat_T1 = os.path.join(dir_o, 'anat_T1.nii.gz')
    if os.path.isfile(anat_T1) == False:
        docmd(['cp',
            os.path.join(dir_i, 'T1w', 'T1w.nii.gz'),
            anat_T1])

    # convert the freesurfer brain mask (made from wmparc)
    anat_T1_brain_mask = os.path.join(dir_o, 'anat_T1_brain_mask.nii.gz')
    if os.path.isfile(anat_T1_brain_mask) == False:
        docmd(['cp',
            os.path.join(dir_i, 'T1w', 'brainmask_fs.nii.gz'),
            anat_T1_brain_mask])

    # multiply the freesurfer brainmask by the T1 to get the _brain.nii.gz
    anat_T1_brain = os.path.join(dir_o, 'anat_T1_brain.nii.gz')
    if os.path.isfile(dir_o + '/anat_T1_brain.nii.gz') == False:
        docmd(['fslmaths', anat_T1,
            '-mul', anat_T1_brain_mask,
            anat_T1_brain])

    # cp APARC atlas
    anat_aparc = os.path.join(dir_o, 'anat_aparc_brain.nii.gz')
    if os.path.isfile(anat_aparc) == False:
        docmd(['cp',
            os.path.join(dir_i,'T1w','aparc+aseg.nii.gz'),
            anat_aparc])

    # cp MGZ APARC2009 atlas
    anat_aparc2009 = os.path.join(dir_o,  'anat_aparc2009_brain.nii.gz')
    if os.path.isfile(anat_aparc2009) == False:
        docmd(['cp',
            os.path.join(dir_i,'T1w','aparc.a2009s+aseg.nii.gz'),
            anat_aparc2009])


    # copy registration of T1 to reg_T1_to_TAL
    TAL_to_T1 = os.path.join(dir_o, 'mat_TAL_to_T1.mat')
    if os.path.isfile(TAL_to_T1) == False:
        docmd(['cp',
            os.path.join(dir_i, 'MNINonLinear','xfms','T1w2StandardLinear.mat'),
            os.path.join(dir_o, 'mat_T1_to_TAL.mat')])
        docmd(['convert_xfm',
                      '-omat', TAL_to_T1,
                      '-inverse',
                      os.path.join(dir_o, 'mat_T1_to_TAL.mat')])

    # copy the useless reg file
    docmd(['cp',
            os.path.join(dir_i, 'MNINonLinear','T1w.nii.gz'),
            os.path.join(dir_o, 'reg_T1_to_TAL.nii.gz')])

    # copy non-linear registration  T1 to MNI
    nlin_warp = os.path.join(dir_o,'reg_nlin_TAL_WARP.nii.gz')
    if os.path.isfile(nlin_warp) == False:
        docmd(['cp', os.path.join(dir_i, 'MNINonLinear','T1w.nii.gz'),
            os.path.join(dir_o, 'reg_nlin_TAL.nii.gz')])
        docmd(['cp',
            os.path.join(dir_i,'MNINonLinear','xfms', 'T1w2Standard_warp_noaffine.nii.gz'),
            nlin_warp])

def main():
    global DEBUG
    global DRYRUN

    arguments = docopt(__doc__)
    hcp_data_dir = arguments['<hcp_data_dir>']
    epi_data_dir = arguments['<epi_data_dir>']
    inputdir = arguments['<nii_inputdir>']
    subjectlist_csv = arguments['<subjectlist_csv>']
    DEBUG = arguments['--debug']
    DRYRUN = arguments['--dry-run']

    if DEBUG: print(arguments)

    ## read in the csv
    if os.path.isfile(subjectlist_csv):
        subjectlist = pd.read_csv(subjectlist_csv)
    else:
        sys.exit("Subjectlist file {} does not exist".format(subjectlist_csv))

    ## loop over the subject list to create directories
    for idx in subjectlist.index:
        anat_id = subjectlist.anat_id[idx]
        rest_id = subjectlist.rest_id[idx]

        ## check that the rest file exists
        rest_nii  = os.path.join(inputdir, subjectlist.input_path[idx])
        if not os.path.exists(rest_nii):
            print("WARNING: input {} does not exist..skipping".format(rest_nii))
            continue

        ## check that the hcp anatomical data directory exists
        hcp_subj_dir = os.path.join(hcp_data_dir, anat_id)
        if not os.path.exists(hcp_subj_dir):
            print("WARNING: input {} does not exist..skipping".format(hcp_subj_dir))
            continue

        ## create the directory structure
        epi_subj_T1dir = os.path.join(epi_data_dir, rest_id, 'T1', 'SESS01')
        epi_subj_funcdir = os.path.join(epi_data_dir, rest_id, 'RST', 'SESS01','RUN01')

        docmd(['mkdir','-p',os.path.join(epi_subj_T1dir, 'RUN01')])
        docmd(['mkdir','-p', epi_subj_funcdir])

        ## copy the rest file over
        docmd(['cp', rest_nii, os.path.join(epi_subj_funcdir, '{}.nii.gz'.format(rest_id))])

        ## copy over all the hcp preprocessed data
        export_from_hcp(hcp_subj_dir, epi_subj_T1dir)


# In[ ]:
if __name__ == "__main__":
    main()
