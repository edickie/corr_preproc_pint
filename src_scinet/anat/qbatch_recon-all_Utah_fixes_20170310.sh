#!/bin/bash

## set the site variable here
SITE='Utah'
NIIDIR=${SCRATCH}/CORR/inputs/Utah

#load the ciftify enviroment
module load gnu-parallel/20140622
module load /home/a/arisvoin/edickie/quarantine/modules/edickie_quarantine
module load extras
module load freesurfer/6.0.0
module load python/2.7.11-Anaconda2-2.5.0 
module load qbatch/1.0.1
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout6/${SITE}


cd ${NIIDIR}
## this was already run
# for anatfile in */session_?/anat_?/anat.nii.gz; 
# do 
#    echo $anatfile; 
#  wb_command -volume-math 'clamp(x, 0, 1000)' $(dirname ${anatfile})/anat_clamped.nii.gz -var x $anatfile; 
# done 


## get the subjects list from the NIIDIR
SUBJECTS=`cd ${NIIDIR}; ls -1d 002????`

## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir -p $SUBJECTS_DIR
cd $SUBJECTS_DIR
## 0026026s1r1 didn't have enough walltime
rm -r 0026026s1r1
parallel "echo recon-all -no-tal-check -subject {}s1r1 -i ${NIIDIR}/{}/session_1/anat_1/anat_clamped.nii.gz -sd ${SUBJECTS_DIR} -all -parallel" ::: 0026026 | \
  ${HOME}/code/qbatch/bin/qbatch --walltime 14:00:00 -c 2 -j 2 --ppj 8 -N fss1${SITE} -

parallel "echo recon-all -no-tal-check -subject {}s2r1 -i ${NIIDIR}/{}/session_2/anat_1/anat_clamped.nii.gz -sd ${SUBJECTS_DIR} -all -parallel" ::: $SUBJECTS | \
  ${HOME}/code/qbatch/bin/qbatch --walltime 14:00:00 -c 2 -j 2 --ppj 8 -N fss2${SITE} -
