#!/bin/bash

## set the site variable here
SITE='UPSM'

#load the epitome enviroment
module load gnu-parallel/20140622
source ${HOME}/myscripts/abide/ciftify_env_nodatapaths.sh
module load qbatch/1.0.1
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout/${SITE}
export HCP_DATA=${SCRATCH}/CORR/hcp/${SITE}
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/data/

## get the subjects list from the NIIDIR
SUBJECTS=`cd ${SUBJECTS_DIR}; ls -1d 002????s?r?`

module load git
## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir -p $HCP_DATA
cd $HCP_DATA
parallel "echo fs2hcp.py {}" ::: $SUBJECTS | \
  qbatch --walltime 2:00:00 -c 4 -j 4 --ppj 8 -N fs2hcp${SITE} -

module load ImageMagick
echo "Submitting the hcp-functional-qc"
echo ${HOME}/myscripts/corr/anat/fs2hcp-qc_CORR_20170309.sh ${HCP_DATA} ${SITE} | \
  qbatch --walltime 5:00:00 -c 1 -j 1 --ppj 8 -N qcraw${SITE} --afterok fs2hcp${SITE} -
