#!/bin/bash

## this little script runs the ciftify freesurfer QC for a project

## set the site variable here
HCP_DATA=${1}
QCMODE=${2}
QCLABLE=${3}
shift
shift
shift
SUBJECTS=${@}


export HCP_DATA=${HCP_DATA}

tmpdir=$(mktemp --tmpdir=/dev/shm -d tmp.XXXXXX)

function cleanup_ramdisk {
    echo -n "Cleaning up ramdisk directory ${tmpdir} on "
    date
    rm -rf ${tmpdir}
    echo -n "done at "
    date
}

#trap the termination signal, and call the function 'trap_term' when
# that happens, so results may be saved.
trap cleanup_ramdisk EXIT

## get the subjects list from the HCP_DATA list
cd ${HCP_DATA}

## run the QC - note it's a little faster because we are running the snaps generation in parallel across subjects
qcdir="qc_${QCLABLE}_${QCMODE}"
parallel -j 4 "cifti-vis-recon-all snaps ${QCMODE} --qcdir ${tmpdir}/${qcdir} {}" ::: $SUBJECTS
cifti-vis-recon-all index ${QCMODE} --qcdir ${tmpdir}/${qcdir}

## move the data from the ramdisk back to HCP_DATA
cd ${tmpdir}
tar -cf ${HCP_DATA}/${qcdir}.tar ${qcdir}/
rm -r ${tmpdir}/${qcdir}
cd ${HCP_DATA}
