#!/bin/bash

## set the site variable here
SITE='UPSM'

#load the epitome enviroment
module load gnu-parallel/20150822
source ${HOME}/myscripts/corr/ciftify_env_nodatapaths.sh
module load qbatch/1.0.1
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout6/${SITE}
export HCP_DATA=${SCRATCH}/CORR/hcp_fs6/${SITE}

## get the subjects list from the NIIDIR
SUBJECTS=`cd ${SUBJECTS_DIR}; ls -1d 002????s?r?`

## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir -p $HCP_DATA
cd $HCP_DATA
parallel "echo fs2hcp.py {}" ::: $SUBJECTS | \
  qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N fs62hcp${SITE} -

echo "Submitting the hcp-functional-qc"
echo ${HOME}/myscripts/corr/anat/fs2hcp-qc_CORR_20170309.sh ${HCP_DATA} ${SITE} | \
  qbatch --walltime 5:00:00 -c 1 -j 1 --ppj 8 -N qcfs6${SITE} --afterok fs62hcp${SITE} -
