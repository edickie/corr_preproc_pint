#!/bin/bash

## set the site variable here
SITE='LMU_2'
NIIDIR=${SCRATCH}/CORR/inputs/LMU_2

#load the ciftify enviroment
module load gnu-parallel/20140622
module load /home/a/arisvoin/edickie/quarantine/modules/edickie_quarantine
module load extras
module load freesurfer/6.0.0
module load python/2.7.11-Anaconda2-2.5.0 
module load qbatch/1.0.1
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout6/${SITE}


## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir -p $SUBJECTS_DIR

## get the session 1 subjects list from the NIIDIR
Session1Files=`cd ${NIIDIR}; ls -1d 002????/session_1/anat_1/anat.nii.gz`
Session1Subjects="0025370 0025383"


## submit the files to the queue
cd $SUBJECTS_DIR

for sub in ${Session1Subjects}; do rm -r ${sub}*; done
parallel "echo recon-all -no-tal-check -subject {}s1r1 -i ${NIIDIR}/{}/session_1/anat_1/anat.nii.gz -sd ${SUBJECTS_DIR} -all" ::: ${Session1Subjects} | \
  qbatch --walltime 30:00:00 -c 2 -j 2 --ppj 8 -N fss1${SITE} -
