#!/bin/bash

## set the site variable here
SITE='UPSM'
NIIDIR=${SCRATCH}/CORR/inputs/UPSM

#load the ciftify enviroment
module load gnu-parallel/20140622
module load /home/a/arisvoin/edickie/quarantine/modules/edickie_quarantine
module load extras
module load freesurfer/6.0.0
module load python/2.7.11-Anaconda2-2.5.0 
module load qbatch/1.0.1
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout6/${SITE}

## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
cd $SUBJECTS_DIR

## get the session 2 subjects list from the NIIDIR
Session2Subjects="0025255
0025257
0025258
0025262
0025263
0025264
0025265
0025266
0025267
0025269
0025270
0025271
0025273
0025274
0025275
0025276
0025277"
for sub in ${Session2Subjects}; 
do 
  rm -r ${sub}s2r1 
done

## submit the files to the queue

parallel "echo recon-all -subject {}s2r1 -i ${NIIDIR}/{}/session_2/anat_1/anat.nii.gz -sd ${SUBJECTS_DIR} -all -parallel -openmp 8" ::: ${Session2Subjects} | \
  qbatch --walltime 16:00:00 -c 1 -j 1 --ppj 8 -N fss2${SITE} -



