#!/bin/bash

## set the site variable here
SITE='Utah'
NIIDIR=${SCRATCH}/CORR/inputs/Utah

#load the ciftify enviroment
module load gnu-parallel/20140622
module load /home/a/arisvoin/edickie/quarantine/modules/edickie_quarantine
module load extras
module load freesurfer/6.0.0
module load python/2.7.11-Anaconda2-2.5.0 
module load qbatch/1.0.1
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout6/${SITE}




## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir -p $SUBJECTS_DIR
cd $SUBJECTS_DIR

rm -r 0026040s2r1
parallel "echo recon-all -no-tal-check -subject {}s2r1 -i ${NIIDIR}/{}/session_2/anat_1/anat_clamped.nii.gz -sd ${SUBJECTS_DIR} -all -parallel" ::: 0026040 | \
  ${HOME}/code/qbatch/bin/qbatch --walltime 20:00:00 -c 2 -j 2 --ppj 8 -N fss2f${SITE} -
