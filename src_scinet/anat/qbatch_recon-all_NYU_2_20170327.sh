#!/bin/bash

## set the site variable here
SITE='NYU_2'
NIIDIR=${SCRATCH}/CORR/inputs/NYU_2

#load the ciftify enviroment
module load gnu-parallel/20140622
module load /home/a/arisvoin/edickie/quarantine/modules/edickie_quarantine
module load extras
module load freesurfer/6.0.0
module load python/2.7.11-Anaconda2-2.5.0 
module load qbatch/1.0.1
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout6/${SITE}

## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir -p $SUBJECTS_DIR

## get the session 1 subjects list from the NIIDIR
Session1Run1Files=`cd ${NIIDIR}; ls -1d 002????/session_1/anat_1/anat.nii.gz`
Session1Run1Subjects=""
for file in ${Session1Run1Files}; 
do 
  sub=$(dirname $(dirname $(dirname ${file}))) 
  Session1Run1Subjects="${Session1Run1Subjects} ${sub}" 
done

## submit the files to the queue
cd $SUBJECTS_DIR
parallel "echo recon-all -subject {}s1r1 -i ${NIIDIR}/{}/session_1/anat_1/anat.nii.gz -sd ${SUBJECTS_DIR} -all -parallel" ::: ${Session1Run1Subjects} | \
  qbatch --walltime 16:00:00 -c 2 -j 2 --ppj 8 -N fss1r1${SITE} -

## get the session 1 run 2 subjects list from the NIIDIR
Session1Run2Files=`cd ${NIIDIR}; ls -1d 002????/session_1/anat_2/anat.nii.gz`
Session1Run2Subjects=""
for file in ${Session1Run2Files}; 
do 
  sub=$(dirname $(dirname $(dirname ${file}))) 
  Session1Run2Subjects="${Session1Run2Subjects} ${sub}" 
done

## submit the files to the queue
cd $SUBJECTS_DIR
parallel "echo recon-all -subject {}s1r2 -i ${NIIDIR}/{}/session_1/anat_2/anat.nii.gz -sd ${SUBJECTS_DIR} -all -parallel" ::: ${Session1Run2Subjects} | \
  qbatch --walltime 16:00:00 -c 2 -j 2 --ppj 8 -N fss1r2${SITE} -

## get the session 2 run 1subjects list from the NIIDIR
Session2Run1Files=`cd ${NIIDIR}; ls -1d 002????/session_2/anat_1/anat.nii.gz`
Session2Run1Subjects=""
for file in ${Session2Run1Files}; 
do 
  sub=$(dirname $(dirname $(dirname ${file}))) 
  Session2Run1Subjects="${Session2Run1Subjects} ${sub}" 
done

## submit the files to the queue
cd $SUBJECTS_DIR
parallel "echo recon-all -subject {}s2r1 -i ${NIIDIR}/{}/session_2/anat_1/anat.nii.gz -sd ${SUBJECTS_DIR} -all -parallel" ::: ${Session2Run1Subjects} | \
  qbatch --walltime 16:00:00 -c 2 -j 2 --ppj 8 -N fss2r1${SITE} -

## get the session 2 run 2 subjects list from the NIIDIR
Session2Run2Files=`cd ${NIIDIR}; ls -1d 002????/session_2/anat_2/anat.nii.gz`
Session2Run2Subjects=""
for file in ${Session2Run2Files}; 
do 
  sub=$(dirname $(dirname $(dirname ${file}))) 
  Session2Run2Subjects="${Session2Run2Subjects} ${sub}" 
done

## submit the files to the queue
cd $SUBJECTS_DIR
parallel "echo recon-all -subject {}s2r2 -i ${NIIDIR}/{}/session_2/anat_2/anat.nii.gz -sd ${SUBJECTS_DIR} -all -parallel" ::: ${Session2Run2Subjects} | \
  qbatch --walltime 16:00:00 -c 2 -j 2 --ppj 8 -N fss2r2${SITE} -

