#!/bin/bash

## set the site variable here
SITE='UPSM'
NIIDIR=${SCRATCH}/CORR/inputs/UPSM

#load the ciftify enviroment
module load gnu-parallel/20140622
module load /home/a/arisvoin/edickie/quarantine/modules/edickie_quarantine
module load extras
module load freesurfer/6.0.0
module load python/2.7.11-Anaconda2-2.5.0 
module load qbatch/1.0.1
export SUBJECTS_DIR=${SCRATCH}/CORR/FSout6/${SITE}

## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir -p $SUBJECTS_DIR

## get the session 1 subjects list from the NIIDIR
Session1Files=`cd ${NIIDIR}; ls -1d 002????/session_1/anat_1/anat.nii.gz`
Session1Subjects=""
for file in ${Session1Files}; 
do 
  sub=$(dirname $(dirname $(dirname ${file}))) 
  Session1Subjects="${Session1Subjects} ${sub}" 
done

## submit the files to the queue
cd $SUBJECTS_DIR
parallel "echo recon-all -subject {}s1r1 -i ${NIIDIR}/{}/session_1/anat_1/anat.nii.gz -sd ${SUBJECTS_DIR} -all -parallel -openmp 8" ::: ${Session1Subjects} | \
  qbatch --walltime 16:00:00 -c 1 -j 1 --ppj 8 -N fss1${SITE} -

## get the session 2 subjects list from the NIIDIR
Session2Files=`cd ${NIIDIR}; ls -1d 002????/session_2/anat_1/anat.nii.gz`
Session2Subjects=""
for file in ${Session2Files}; 
do 
  sub=$(dirname $(dirname $(dirname ${file}))) 
  Session2Subjects="${Session2Subjects} ${sub}" 
done

## submit the files to the queue
cd $SUBJECTS_DIR
parallel "echo recon-all -subject {}s2r1 -i ${NIIDIR}/{}/session_2/anat_1/anat.nii.gz -sd ${SUBJECTS_DIR} -all -parallel -openmp 8" ::: ${Session2Subjects} | \
  qbatch --walltime 16:00:00 -c 1 -j 1 --ppj 8 -N fss2${SITE} -

## get the session 3 subjects list from the NIIDIR
Session3Files=`cd ${NIIDIR}; ls -1d 002????/session_3/anat_1/anat.nii.gz`
Session3Subjects=""
for file in ${Session3Files}; 
do 
  sub=$(dirname $(dirname $(dirname ${file}))) 
  Session3Subjects="${Session3Subjects} ${sub}" 
done

## submit the files to the queue
cd $SUBJECTS_DIR
parallel "echo recon-all -no-tal-check -subject {}s3r1 -i ${NIIDIR}/{}/session_3/anat_1/anat.nii.gz -sd ${SUBJECTS_DIR} -all -parallel -openmp 8" ::: ${Session3Subjects} | \
  qbatch --walltime 16:00:00 -c 1 -j 1 --ppj 8 -N fss3${SITE} -


