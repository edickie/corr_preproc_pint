## 2017-10-05 we reran all the ciftify_subject_fmri with
ciftify version 1.0.0 because there was a left-right projecting bug in the old func2hcp

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=LMU_2
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}

acompor_rst=`ls 002*/MNINonLinear/Results/*acompcor/native/002*lowpass_rest0?.nii.gz`

for rstfile in ${acompor_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 echo ciftify_subject_fmri ${rstfile} ${Subject} ${NameOffMRI}
done | \
  qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N cfyacompcor${SITE} -

fix_rst=`ls 002*/MNINonLinear/Results/sess?rest?_fix/native/func_fix.20160422.01.nii.gz`
for rstfile in ${fix_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 echo ciftify_subject_fmri ${rstfile} ${Subject} ${NameOffMRI}
done | \
  qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N cfyfix${SITE} -
```
## rerunning UPSM

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=UPSM
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}

acompor_rst=`ls 002*/MNINonLinear/Results/*acompcor/native/002*lowpass_rest0?.nii.gz`

for rstfile in ${acompor_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 echo ciftify_subject_fmri ${rstfile} ${Subject} ${NameOffMRI}
done | \
  qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N cfyacompcor${SITE} -

fix_rst=`ls 002*/MNINonLinear/Results/sess?rest?_fix/native/func_fix.20160422.01.nii.gz`
for rstfile in ${fix_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 echo ciftify_subject_fmri ${rstfile} ${Subject} ${NameOffMRI}
done | \
  qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N cfyfix${SITE} -
```

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=Utah
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}

acompor_rst=`ls 002*/MNINonLinear/Results/*acompcor/native/002*lowpass_rest0?.nii.gz`

for rstfile in ${acompor_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 echo ciftify_subject_fmri ${rstfile} ${Subject} ${NameOffMRI}
done | \
  qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N cfyacompcor${SITE} -

fix_rst=`ls 002*/MNINonLinear/Results/sess?rest?_fix/native/func_fix.20160422.01.nii.gz`
for rstfile in ${fix_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 echo ciftify_subject_fmri ${rstfile} ${Subject} ${NameOffMRI}
done | \
  qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N cfyfix${SITE} -
```

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=NYU_2
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}

fix_rst=`ls 002*/MNINonLinear/Results/sess?rest?_fix/native/func_fix.20160422.01.nii.gz`
for rstfile in ${fix_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 echo ciftify_subject_fmri ${rstfile} ${Subject} ${NameOffMRI}
done | \
  qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N cfyfix${SITE} -
```

# 2017-10-13

So it appears that nothing finished...everything froze when it hit cifti-resample..boo
My guess it some kind of lock on the RAM

I'm gonna try to rerun everything with only one subject per node..

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=NYU_2
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}

fix_rst=`ls 002*/MNINonLinear/Results/sess?rest?_fix/native/func_fix.20160422.01.nii.gz`

for rstfile in ${fix_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 rm ${Subject}/MNINonLinear/Results/${NameOffMRI}/${NameOffMRI}.nii.gz
 rm ${Subject}/MNINonLinear/Results/${NameOffMRI}/ciftify_subject_fmri.log
 rm ${Subject}/MNINonLinear/Results/${NameOffMRI}/native/*mat
done

for rstfile in ${fix_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 echo ciftify_subject_fmri ${rstfile} ${Subject} ${NameOffMRI}
done | \
  qbatch --walltime 1:00:00 -c 1 -j 1 --ppj 8 -N cfyfix${SITE} -
```
## also run this for LMU_2 and UPSM..

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=Utah
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}

fix_rst=`ls 002*/MNINonLinear/Results/sess?rest?_fix/native/func_fix.20160422.01.nii.gz`

for rstfile in ${fix_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 rm ${Subject}/MNINonLinear/Results/${NameOffMRI}/${NameOffMRI}.nii.gz
 rm ${Subject}/MNINonLinear/Results/${NameOffMRI}/ciftify_subject_fmri.log
 rm ${Subject}/MNINonLinear/Results/${NameOffMRI}/native/*mat
done

for rstfile in ${fix_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 echo ciftify_subject_fmri ${rstfile} ${Subject} ${NameOffMRI}
done | \
  qbatch --walltime 1:00:00 -c 1 -j 1 --ppj 8 -N cfyfix${SITE} -
```

## also run this for LMU_2 and UPSM..

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=LMU_2
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}

acompor_rst=`ls 002*/MNINonLinear/Results/*acompcor/native/002*lowpass_rest0?.nii.gz`

for rstfile in ${acompor_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 rm ${Subject}/MNINonLinear/Results/${NameOffMRI}/${NameOffMRI}.nii.gz
 rm ${Subject}/MNINonLinear/Results/${NameOffMRI}/ciftify_subject_fmri.log
 rm ${Subject}/MNINonLinear/Results/${NameOffMRI}/native/*mat
done

for rstfile in ${acompor_rst}; do
 Subject=$(basename $(dirname $(dirname $(dirname $(dirname $(dirname ${rstfile}))))))
 NameOffMRI=$(basename $(dirname $(dirname ${rstfile})))
 echo ciftify_subject_fmri ${rstfile} ${Subject} ${NameOffMRI}
done | \
  qbatch --walltime 1:00:00 -c 1 -j 1 --ppj 8 -N cfyacompcor${SITE} -
```

## running qc

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=LMU_2
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}


parallel "echo ${HOME}/myscripts/corr/ciftify_subject_fmri_201710/vis_fmri_20171014.sh ${HCP_DATA} ${SITE}-{} 0025{}?s?r?" \
 ::: 36 37 38 39 40 | \
  qbatch --walltime 1:30:00 -c 1 -j 1 --ppj 8 -N qcfmri${SITE} -

```
```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=NYU_2
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}

parallel "echo ${HOME}/myscripts/corr/ciftify_subject_fmri_201710/vis_fmri_20171014.sh ${HCP_DATA} ${SITE}-{} 0025{}?s?r?" \
 ::: 00 01 02 03 04 05 07 08 09 10 11 12 13 14 15 16 17 18 | \
  qbatch --walltime 2:00:00 -c 1 -j 1 --ppj 8 -N qcfmri${SITE} -
```

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=UPSM
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}

parallel "echo ${HOME}/myscripts/corr/ciftify_subject_fmri_201710/vis_fmri_20171014.sh ${HCP_DATA} ${SITE}-{} 0025{}?s?r?" \
 ::: 23 24 25 26 27 28 29 30 31 32 33 | \
  qbatch --walltime 4:00:00 -c 1 -j 1 --ppj 8 -N qcfmri${SITE} -
```

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

SITE=Utah
export HCP_DATA=/scratch/a/arisvoin/edickie/CORR/hcp_fs6/${SITE}
cd ${HCP_DATA}

parallel "echo ${HOME}/myscripts/corr/ciftify_subject_fmri_201710/vis_fmri_20171014.sh ${HCP_DATA} ${SITE}-{} 0026{}?s?r?" \
 ::: 01 02 03 04 | \
  qbatch --walltime 3:00:00 -c 1 -j 1 --ppj 8 -N qcfmri${SITE} -
```
## Next Steps

+ QC page generation
+ smoothing with 2 4 6 8 10 12
