## scinet modules
module load Xlibraries ## this is now a prereq for connectome-workbench
module load git
module load ImageMagick

## in the scinet biomed section
module load use.experimental
module load freesurfer/6.0.0
module load fsl/5.0.10
module load connectome-workbench/1.2.3

## my ciftify set-up
module load anaconda3
source activate /scinet/course/ss2017/16_mripython/conda_envs/mripython3.6

export PATH=${PATH}:${HOME}/code/ciftify/ciftify/bin
export PYTHONPATH=${PYTHONPATH}:${HOME}/code/ciftify/

export HCP_SCENE_TEMPLATES=${HOME}/code/ciftify/ciftify/data/scene_templates
export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data/
