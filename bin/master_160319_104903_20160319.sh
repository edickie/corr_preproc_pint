#!/bin/bash

# Master script for NYU: RST.
# Generated: 2016/03/19 -- 10:49:03 by edickie.

## Setup
export DIR_PIPE=/home/a/arisvoin/edickie/epi-clone/20160319
export DIR_DATA=/scratch/a/arisvoin/edickie/ABIDE/epi-fixtrain
export DIR_EXPT=MaxMun
export DATA_TYPE=RST
export ID=20160319

export PROC=${DIR_DATA}/${DIR_EXPT}/proclist_160319_104903_20160319.sh

export SUBJECTS=$(epi-listsubj ${DIR_DATA}/${DIR_EXPT})

## Freesurfer
epi-fsrecon ${DIR_DATA} ${DIR_EXPT} ${PROC}
## Freesurfer to HCP conversion
epi-fsrecon2hcp ${DIR_DATA} ${DIR_EXPT} ${PROC}
. ${DIR_PIPE}/modules/hcp/hcpexport ${DIR_DATA} ${DIR_EXPT} >> ${PROC}

## Begin Pipeline
for SUB in ${SUBJECTS}; do

export CMD=${DIR_DATA}/${DIR_EXPT}/${SUB}/cmd_160319_104903_20160319.sh
cat > ${CMD} << EOF
#!/bin/bash
set -e

export DIR_PIPE=${DIR_PIPE}
export DIR_DATA=${DIR_DATA}
export DIR_EXPT=${DIR_EXPT}
export DATA_TYPE=${DATA_TYPE}
export ID=${ID}
export SUB=${SUB}
McRetroTS='/home/a/arisvoin/edickie/epi-clone/20160319/bin/run_McRetroTS.sh None'
EOF

. ${DIR_PIPE}/modules/pre/init_basic high 3 >> ${CMD}
. ${DIR_PIPE}/modules/pre/despike del >> ${CMD}
. ${DIR_PIPE}/modules/pre/slice_time_correct despike alt+z >> ${CMD}
. ${DIR_PIPE}/modules/pre/motion_deskull tshift normal FSL >> ${CMD}
. ${DIR_PIPE}/modules/pre/scale deskull scale  >> ${CMD}
. ${DIR_PIPE}/modules/pre/reg_calc_hcpfsl high corratio 12 >> ${CMD}
. ${DIR_PIPE}/modules/pre/detrend scaled 1 >> ${CMD}
. ${DIR_PIPE}/modules/pre/calc_dvars detrend >> ${CMD}
. ${DIR_PIPE}/modules/pre/ica detrend EPI_mask >> ${CMD}
. ${DIR_PIPE}/modules/pre/ica_fix detrend /home/a/arisvoin/edickie/quarantine/FIX/1.06/build/training_files/Standard.RData 20 on off >> ${CMD}

chmod 750 ${CMD}
# append this subject to the process queue
echo ${CMD} >> ${PROC}
done

# calls to QC programs
. ${DIR_PIPE}/modules/qc/qc_epi2t1 ${DIR_DATA} ${DIR_EXPT} ${DATA_TYPE} >> ${PROC}
. ${DIR_PIPE}/modules/qc/qc_mctrs ${DIR_DATA} ${DIR_EXPT} ${DATA_TYPE} ${ID} >> ${PROC}
. ${DIR_PIPE}/modules/qc/qc_motionind ${DIR_DATA} ${DIR_EXPT} ${DATA_TYPE} ${ID} >> ${PROC}
. ${DIR_PIPE}/modules/qc/qc_t12mni ${DIR_DATA} ${DIR_EXPT} ${DATA_TYPE} >> ${PROC}

