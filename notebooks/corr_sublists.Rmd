---
title: "R Notebook"
output:
  pdf_document: default
font: Calibri Light
---

```{r, message = F}
library(dplyr)
library(tableone)
```

```{r}

set.seed(1)

LMU_sublist <- read.csv('../phenotypic_data/LMU_2_phenotypic_data.csv')
       
repvars <- c("AGE_AT_SCAN_1", "SEX", "DSM_IV_TR")

LMU_dems <- LMU_sublist %>%
  filter(SESSION == "Baseline") %>%
  select(SUBID, one_of(repvars))

LMU_sublist <- merge(select(LMU_sublist, -one_of(repvars)), LMU_dems, by = "SUBID")
LMU_sublist$AGE_AT_SCAN_1 <- as.numeric(as.character(LMU_sublist$AGE_AT_SCAN_1))

LMU_anat_blacklist <- c("0025362s1r1","0025383s1r1")

LMU_sublist$anat_id <- paste0("00",LMU_sublist$SUBID, 's1r1')
LMU_sublist$rest_id[LMU_sublist$SESSION == "Baseline"] <- paste0("00",LMU_sublist$SUBID[LMU_sublist$SESSION == "Baseline"], 's1r1')
LMU_sublist$rest_id[LMU_sublist$SESSION == "Retest_1"] <- paste0("00",LMU_sublist$SUBID[LMU_sublist$SESSION == "Retest_1"], 's1r2')
LMU_sublist$rest_id[LMU_sublist$SESSION == "Retest_2"] <- paste0("00",LMU_sublist$SUBID[LMU_sublist$SESSION == "Retest_2"], 's1r3')
LMU_sublist$rest_id[LMU_sublist$SESSION == "Retest_3"] <- paste0("00",LMU_sublist$SUBID[LMU_sublist$SESSION == "Retest_3"], 's1r4')

LMU_sublist$GROUP <- NA
LMU_sublist$GROUP[LMU_sublist$DSM_IV_TR == "Mild cognitive impairment"] <- "MCI"
LMU_sublist$GROUP[LMU_sublist$DSM_IV_TR != "Mild cognitive impairment" & LMU_sublist$AGE_AT_SCAN_1 > 50] <- "OlderAdult"
LMU_sublist$GROUP[LMU_sublist$DSM_IV_TR != "Mild cognitive impairment" & LMU_sublist$AGE_AT_SCAN_1 < 50] <- "YoungerAdult"

LMU_sublist_qc <- LMU_sublist %>%
  filter(!anat_id %in% LMU_anat_blacklist) %>%  
  ungroup() %>%
  mutate(input_path = paste0(substr(rest_id, 1,7),
                            '/session_',
                            substr(rest_id, 9,9),
                            '/rest_',
                            substr(rest_id, 11, 11),
                            '/rest.nii.gz'))

## 2017-04-20 some runs where poor for training..excluding them from training set
poor_fixtrainers <-  c('25368', '25386', '25378')
traininglist <- LMU_sublist_qc %>%
  filter(!(SUBID %in% poor_fixtrainers)) %>%
  group_by(SUBID) %>%
  sample_n(1) %>%
  ungroup() %>%
  group_by(GROUP) %>%
  sample_n(8)

CreateTableOne(vars = c("AGE_AT_SCAN_1","SEX", "SESSION"),
               factorVars = c("SEX", "SESSION"),
               strata = "GROUP",
               data = traininglist)

write.csv(traininglist, "../sublists/LMU_2_fix_training_list.csv", row.names = F)
write.csv(LMU_sublist_qc, "../sublists/LMU_2_anat_qced_list.csv", row.names = F)

```

## the Utah sample has two sessions and three resting state runs

+ Session 1 has an anat and rest1
+ Session 2 has 1 anat and 2 rests (so immediate test-retest)

Since there are only 27 participants, we will take chose one random run from each of them

No scans were blacklisted during Freesurfer QC

```{r}
Utah_sublist <- read.csv('../phenotypic_data/Utah_1_phenotypic_data.csv')

Utah_sublist$anat_id <- NA
Utah_sublist$rest_id <- NA

Utah_sublist$anat_id[Utah_sublist$SESSION == "Baseline"] <- paste0('00',                      Utah_sublist$SUBID[Utah_sublist$SESSION == "Baseline"], 's1r1')
Utah_sublist$rest_id[Utah_sublist$SESSION == "Baseline"] <-  paste0("00",Utah_sublist$SUBID[Utah_sublist$SESSION == "Baseline"], 's1r1')

Utah_sublist$anat_id[Utah_sublist$SESSION == "Retest_1"] <- paste0('00',                      Utah_sublist$SUBID[Utah_sublist$SESSION == "Retest_1"], 's2r1')
Utah_sublist$rest_id[Utah_sublist$SESSION == "Retest_1"] <-  paste0("00",Utah_sublist$SUBID[Utah_sublist$SESSION == "Retest_1"], 's2r1')

Utah_sublist$anat_id[Utah_sublist$SESSION == "Retest_2"] <- paste0('00',                      Utah_sublist$SUBID[Utah_sublist$SESSION == "Retest_2"], 's2r1')
Utah_sublist$rest_id[Utah_sublist$SESSION == "Retest_2"] <-  paste0("00",Utah_sublist$SUBID[Utah_sublist$SESSION == "Retest_2"], 's2r2')

Utah_sublist <- Utah_sublist %>%
  ungroup() %>%
  mutate(input_path = paste0(substr(rest_id, 1,7),
                            '/session_',
                            substr(rest_id, 9,9),
                            '/rest_',
                            substr(rest_id, 11, 11),
                            '/rest.nii.gz'))

set.seed(1)
Utah_traininglist <- Utah_sublist %>%
  group_by(SUBID) %>%
  sample_n(1) 

CreateTableOne(vars = c("AGE_AT_SCAN_1","SEX", "SESSION"),
               factorVars = c("SEX", "SESSION"),
               data = Utah_sublist)

CreateTableOne(vars = c("AGE_AT_SCAN_1","SEX", "SESSION"),
               factorVars = c("SEX", "SESSION"),
               data = Utah_traininglist)

write.csv(Utah_traininglist, "../sublists/Utah_fix_training_list.csv", row.names = F)
write.csv(Utah_sublist, "../sublists/Utah_anat_qced_list.csv", row.names = F)

```
## UPSM 

UPSM has one rest run per session

All participants have at least 2 sessions, some have 3 sessions

```{r}
UPSM_sublist <- read.csv('../phenotypic_data/UPSM_1_phenotypic_data.csv')

UPSM_anat_blacklist <- c('0025267s2r1',
'0025268s2r1',
'0025298s1r1',
'0025318s2r1')

UPSM_sublist$anat_id <- NA
UPSM_sublist$rest_id <- NA

UPSM_sublist$anat_id[UPSM_sublist$SESSION == "Baseline"] <- paste0('00',                      UPSM_sublist$SUBID[UPSM_sublist$SESSION == "Baseline"], 's1r1')
UPSM_sublist$rest_id[UPSM_sublist$SESSION == "Baseline"] <-  paste0("00",UPSM_sublist$SUBID[UPSM_sublist$SESSION == "Baseline"], 's1r1')

UPSM_sublist$anat_id[UPSM_sublist$SESSION == "Retest_1"] <- paste0('00',                      UPSM_sublist$SUBID[UPSM_sublist$SESSION == "Retest_1"], 's2r1')
UPSM_sublist$rest_id[UPSM_sublist$SESSION == "Retest_1"] <-  paste0("00",UPSM_sublist$SUBID[UPSM_sublist$SESSION == "Retest_1"], 's2r1')

UPSM_sublist$anat_id[UPSM_sublist$SESSION == "Retest_2"] <- paste0('00',                      UPSM_sublist$SUBID[UPSM_sublist$SESSION == "Retest_2"], 's3r1')
UPSM_sublist$rest_id[UPSM_sublist$SESSION == "Retest_2"] <-  paste0("00",UPSM_sublist$SUBID[UPSM_sublist$SESSION == "Retest_2"], 's3r1')

UPSM_sublist <- UPSM_sublist %>%
  ungroup() %>%
  mutate(input_path = paste0(substr(rest_id, 1,7),
                            '/session_',
                            substr(rest_id, 9,9),
                            '/rest_',
                            substr(rest_id, 11, 11),
                            '/rest.nii.gz'))

set.seed(1)
UPSM_traininglist <- UPSM_sublist %>%
  filter(!anat_id %in% UPSM_anat_blacklist) %>%   
  group_by(SUBID) %>%
  sample_n(1) %>%
  ungroup() %>%
  group_by(SESSION) %>%
  sample_n(10)

UPSM_sublist$anat_id[UPSM_sublist$anat_id == '0025267s2r1'] <- '0025267s3r1'
UPSM_sublist$anat_id[UPSM_sublist$anat_id =='0025268s2r1'] <- '0025268s1r1'
UPSM_sublist$anat_id[UPSM_sublist$anat_id =='0025298s1r1'] <- '0025298s2r1'
UPSM_sublist$anat_id[UPSM_sublist$anat_id =='0025318s2r1'] <- '0025318s1r1'

CreateTableOne(vars = c("AGE_AT_SCAN_1","SEX", "SESSION"),
               factorVars = c("SEX", "SESSION"),
               data = UPSM_sublist)

CreateTableOne(vars = c("AGE_AT_SCAN_1","SEX", "SESSION"),
               factorVars = c("SEX", "SESSION"),
               data = UPSM_traininglist)

write.csv(UPSM_traininglist, "../sublists/UPSM_fix_training_list.csv", row.names = F)
write.csv(UPSM_sublist, "../sublists/UPSM_anat_qced_list.csv", row.names = F)

```
## NYU_2 has a rather complex design (one or two anat scans per subject)

+ to simplefy it, we will go with anat_1 for all sessions, unless it failed
+ I needed to manually edit the phenotypic file because some extra scans were present that are not mentioned in the phenotypic file, and the design was too variable to make assumption in phenotypic. We are now reading phenotypic data from this edited file.
+ we are not going to use session 3 at this time

```{r}
NYU_2_sublist <- read.csv('../sublists/NYU_2_phenotypic_ewd20170506.csv')
  
repvars <- c("AGE_AT_SCAN_1", "SEX", "DSM_IV_TR","FIQ","VIQ","PIQ","BMI",                   "SELF_HW","RESTING_STATE_INSTRUCTION","VISUAL_STIMULATION_CONDITION")

NYU_dems <- NYU_2_sublist %>%
  filter(SESSION == "Baseline") %>%
  select(SUBID, one_of(repvars))

NYU_2_sublist <- merge(select(NYU_2_sublist, -one_of(repvars)), NYU_dems, by = "SUBID")
NYU_2_sublist$AGE_AT_SCAN_1 <- as.numeric(as.character(NYU_2_sublist$AGE_AT_SCAN_1))
NYU_2_sublist$FIQ <- as.numeric(as.character(NYU_2_sublist$FIQ))

NYU_anat_blacklist <- c('0025006s1r2',
'0025008s2r2',
'0025019s2r1',
'0025025s1r1',
'0025051s2r2',
'0025084s1r1',
'0025086s1r1',
'0025114s1r2',
'0025175s1r1')

NYU_2_sublist$anat_id <- NA

NYU_2_sublist$input_path <- NYU_2_sublist$file

NYU_2_sublist$rest_id <-paste0(substr(NYU_2_sublist$input_path, 1,7),
                                's', substr(NYU_2_sublist$input_path, 17,17),
                                'r', substr(NYU_2_sublist$input_path, 24,24))

NYU_2_sublist$anat_id <- paste0(substr(NYU_2_sublist$rest_id, 1, 9), 'r1')                

## The training list was selected using an earlier version we read it here so to build the tables
NYU_2_traininglist <- read.csv("../sublists/NYU_2_fix_training_list.csv")

# set.seed(1)
# NYU_2_traininglist <- NYU_2_sublist %>%
#   filter(!anat_id %in% NYU_anat_blacklist) %>%   
#   group_by(SUBID) %>%
#   sample_n(1) %>%
#   ungroup() %>%
#   sample_n(30)


NYU_2_sublist$anat_id[NYU_2_sublist$anat_id =='0025019s2r1'] <- '0025019s2r2'
NYU_2_sublist$anat_id[NYU_2_sublist$anat_id =='0025025s1r1'] <- '0025025s2r1'
# NYU_2_sublist$anat_id[NYU_2_sublist$anat_id =='0025084s1r1'] <- '0025084s1r2'
NYU_2_sublist$anat_id[NYU_2_sublist$anat_id =='0025086s1r1'] <- '0025086s1r2'
NYU_2_sublist$anat_id[NYU_2_sublist$anat_id =='0025175s1r1'] <- '0025175s1r2'

NYU_2_sublist <- NYU_2_sublist %>%
  filter(anat_id != '0025084s1r1')

CreateTableOne(vars = c("AGE_AT_SCAN_1","SEX", "SESSION", "FIQ", "EYE_STATUS_AT_REST_SCAN"),
               factorVars = c("SEX", "SESSION", "EYE_STATUS_AT_REST_SCAN"),
               data = NYU_2_sublist)

CreateTableOne(vars = c("AGE_AT_SCAN_1","SEX", "SESSION", "FIQ", "EYE_STATUS_AT_REST_SCAN"),
               factorVars = c("SEX", "SESSION", "EYE_STATUS_AT_REST_SCAN"),
               data = NYU_2_traininglist)

## the training list was written with an earlier version of this script..
# write.csv(NYU_2_traininglist, "../sublists/NYU_2_fix_training_list.csv", row.names = F)
write.csv(NYU_2_sublist, "../sublists/NYU_2_anat_qced_list.csv", row.names = F)
```
