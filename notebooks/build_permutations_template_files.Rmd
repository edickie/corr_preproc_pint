---
title: "Build templates for PINT Permutations"
output: html_notebook
---

There's a couple of files we would need...

+ joblist file for the smoothing step (<with a SigmaKernel> to sed in..)
+ joblist file for running PINT (several set params)
+ file with paths to summary csvs for postpint steps..
+ paths for testretest calc.. col_headers = c("subid", "run1", "run2")

```{r}

testing_params <- 
within_sess_subfile <- 
```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).
