---
title: "Motion Effect on Test-Retest"
output:
  pdf_document: default
  html_notebook: default
---

Earlier today, I identified a parameter sets that seems to work best for test-retest (of those tested) Here, we will interogate these for effects of mean_fd.

```{r}
library(tidyverse)
library(stringr)
```

```{r}
tot_ivertex_dist_6 <- read_csv("../pint_results_fwhm8-6-6-12/wthsess_all/distance_fingerprint_ivertex_locations_total.csv")
tot_ivertex_dist_10 <- read_csv("../pint_results_fwhm8_10-10-30/wthsess_all/distance_fingerprint_ivertex_locations_total.csv")
wthsess_pheno <- read_csv("~/code/corr_preproc/sublists/within_session_testretest_subjects.csv")
```
```{r}
tot_ivertex_dist <- tot_ivertex_dist_6 %>%
  mutate(search_mm = 6) %>%
  bind_rows(mutate(tot_ivertex_dist_10, search_mm = 10)) %>%
  inner_join(wthsess_pheno, by = c("sub" = "subject"))
```

## look at the stuff vs mean_fd??

```{r}
ggplot(tot_ivertex_dist, aes(x = func_mean_fd_pT, 
                             y = within_med_popZ,
                             color = site)) +
  geom_point() +
  geom_smooth(method = "lm") +
  facet_wrap(~search_mm)
```
```{r}
library(broom)
library(knitr)

lm_func_mean_df <- tot_ivertex_dist %>%
  group_by(search_mm, site) %>%
  do(glance(lm(within_med_popZ ~ func_mean_fd_pT, data = .)))

kable(lm_func_mean_df)
```
```{r}
ggplot(tot_ivertex_dist, aes(x = func_dvars_pT, 
                             y = within_med_popZ,
                             color = site)) +
  geom_point() +
  geom_smooth(method = "lm") +
  facet_wrap(~search_mm)
```

```{r}

lm_func_mean_df <- tot_ivertex_dist %>%
  group_by(search_mm, site) %>%
  do(glance(lm(within_med_popZ ~ Comp.2, data = .)))

kable(lm_func_mean_df)
```
```{r}
ggplot(tot_ivertex_dist, aes(x = func_num_fd, 
                             y = within_med_popZ,
                             color = site)) +
  geom_point() +
  geom_smooth(method = "lm") +
  facet_wrap(~search_mm)
```
```{r}
library(purrr)
UPSM_btw_totdist <- read_csv("~/code/corr_preproc/pint_results_fwhm8_10-10-30/btwsess_NYU_2/distance_fingerprint_ivertex_locations_total.csv")
NYU_btw_totdist <- read_csv("~/code/corr_preproc/pint_results_fwhm8_10-10-30/btwsess_UPSM/distance_fingerprint_ivertex_locations_total.csv")

distfiles <- list.files(path = '../pint_results_fwhm8_10-10-30/', pattern = 'distance_fingerprint_ivertex_locations_total', recursive = TRUE)

distancetot_results <- as.data.frame(distfiles) %>%
  mutate(filepath = file.path('../pint_results_fwhm8_10-10-30/',distfiles),
    res = map(filepath, ~read_csv(.x))) %>%
  unnest()
```


```{r fig.height=10}
distancetot_results %>%
  filter(distfiles %in% c("btwsess_NYU_2/distance_fingerprint_ivertex_locations_total.csv",
                         "btwsess_Utah/distance_fingerprint_ivertex_locations_total.csv",
                         "wthsess_NYU_2/distance_fingerprint_ivertex_locations_total.csv",
                         "wthsess_Utah/distance_fingerprint_ivertex_locations_total.csv" )) %>%
ggplot(aes(x = within_med_popZ, color = distfiles)) +
  geom_density() 
```

```{r}
distfiles
```

```{r}
distancetot_results %>%
  group_by(distfiles) %>%
  summarise_all(.funs = c(mean, sd, median))
```

```{r}
distfiles6 <- list.files(path = '../pint_results_fwhm8-6-6-12/', pattern = 'distance_fingerprint_ivertex_locations_total', recursive = TRUE)

distancetot_results6 <- as.data.frame(distfiles) %>%
  mutate(filepath = file.path('../pint_results_fwhm8-6-6-12/',distfiles),
    res = map(filepath, ~read_csv(.x))) %>%
  unnest()
```
```{r fig.height=10}
distancetot_results6 %>%
  filter(distfiles %in% c("btwsess_NYU_2/distance_fingerprint_ivertex_locations_total.csv",
                         "btwsess_Utah/distance_fingerprint_ivertex_locations_total.csv",
                         "wthsess_NYU_2/distance_fingerprint_ivertex_locations_total.csv",
                         "wthsess_Utah/distance_fingerprint_ivertex_locations_total.csv" )) %>%
ggplot(aes(x = within_med_popZ, color = distfiles)) +
  geom_density() 
```
Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).
